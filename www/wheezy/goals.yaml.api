# * perl * (actually pod)

=head1 NAME

goals.yaml - Release goal data

=head1 SYNOPSIS

This document describes the format of the machine parsable release
goals file, "goals.yaml".

=head1 DESCRIPTION

The goals.yaml file describes release goals and consists of one "YAML
document".  Extra documents I<may> be added at the end in a later
version of this API, implementations should handle this.

The YAML document is an associative array with the fields listed
below.  Unknown fields should be ignored by implementations.

=over 4

=item codename

The codename of the Debian release the goals.yaml file is describing.

=item last-updated

Time of last modification.  Format is the based on the date format
described in RFC-822.

=item release-goals

This is a sequence of release goals.  See L</Release goals> below.

=back

=head2 Release goals

The order of the elements in this sequence is not significant nor is
it guaranteed to be stable.  Each element is a goal and represented as
an associative array.  The accepted fields are listed below.
Implementations should ignore unknown fields.

=over 4

=item name

This is the "Human Readable" name for the goal.

=item advocates

List of the names of advocates (order not significant).

=item state

The state of the release goal.

=over 4

=item completed

The release goal is declared as completed.

=item accepted

The release goal is valid and has been accepted for the current
release.

=item proposed

The release goal has been proposed, but has not neither been rejected
nor accepted at this point.

=item withdrawn

The release goal was withdrawn by the proposers.

=item rejected

The release goal has been rejected (or recalled) by the release team.

=back

=item url

This is either the URL to the wiki/webpage page describing the goal
I<or> the special value "N/A".  In the latter case, there is no such
page.

=item bugs

If present, this is an associative array.  The contents can be used to
locate usertagged bugs associated with this release goal.

=over 4

=item user

The contents for the "user" field for the Debian BTS.

=item usertags

List of usertags.

=back

=back


