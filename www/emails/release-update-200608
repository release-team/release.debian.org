DRAFT!

Subject: release update: freeze, RC Bug count, python, toolchain

Hi,

the first packages have been frozen! Whoooa.

Although we all have been waiting for this to happen for a quite some
time now, this doesn't mean all is set.  We have a very tough time
ahead of us to actually release in time in December 2006, and we
all have to set the release as priority one in order to make it happen.
We especially have to give it priority over flame wars.

As we're getting nearer to the release, you will at least see a monthly
release update from us from now on.  If you want to see only a few, help
us to release etch in time!

Also, now is the right time to start worrying about the release notes. If
you have any issues you would like to see covered, please file a bug
report against the release-notes pseudo-package now.  If possible, please
include a (short) proposed text.


RC bug count
============

We are still seeing many uncoordinated uploads to unstable.  These often
tie up different transitions and block them.  Currently, a lot of fixes
are kept in unstable, as they're tied to the perl and the neon transition;
as perl is starting to look better, it might be possible to clear this
transition during the next few days.

If you are maintaining a library package with some reverse dependencies,
we would like you to coordinate every SONAME-changing upload with the
release team.  We have talked to the ftp-team to be notified if such
uploads are in NEW, but we would prefer if you would talk to us earlier.

Even if you don't change the SONAME, be aware that your package may be a
blocker for others, so waiting a few days often helps all of us.  Several
websites help to check if your package is involved in any transitions [1].
If you are unsure, you can always ask the release team.

The RC bug tracker shows about 354/270 as of today release-critical bugs -
that is way too much (the first number is the number of RC bugs in etch,
and the second number is the number of RC bugs that are neither fixed in
etch nor in unstable).  Please help us reduce this number.  At this
point, we want to remind you that we have a permanent BSP: You can 
upload 0-days NMUs for RC-bugs open for more than one week.  However,
you are still required to notify the maintainer via BTS before uploading.
And of course, you need to take care of anything you broke by your NMU.
Please upload security bug fixes with urgency high, and other RC bug
fixes with urgency medium - as of writing this, we have almost 70 bug
solutions waiting for testing propagation, which is a bit too much.

Please see the Developers Reference for more details about NMUing [2].

Python
======

The python transition is still going on, please see the recent mail
to debian-devel-announce for details.[4]


gcc-4.X transition
==================

Has been finished for some time. The few remaining bugs with the new
compiler (mostly packages failing to build due to stricter checking)
should be fixed in the next few weeks and are mostly removed from etch
already.


Toolchain and base freeze
=========================

The toolchain is mostly in the stable state we would like to see it in
for etch. There are, however, some issues on hppa which cause us some
worries.


We have frozen the essential toolchain. Here's the list of frozen
packages:
* binutils
* build-essential
* dpkg
* gcc-defaults
* gcc-4.1
* gcc-4.0 
* glibc 
* make-dfsg

We will also soon freeze non-essential toolchain packages that are commonly
used in the build process. The initial list of non-essential toolchain
packages is:
* debhelper
* cdbs
* bison
* python2.4
* gcj


Packages with priority required or important are expected to be frozen next
week. The number of involved packages is 110 [3].

The canonical list of frozen packages is available from
http://ftp-master.debian.org/testing/hints/freeze



Full IPv6 support
=================

There has been some confusion about the Etch release goal about IPv6. Our
understanding of that release goal is that all network applications should be
able to work with both IPv4 and IPv6. Also stateful packet filtering should
work for both protocols. Please consider all bugs tagged "ipv6" to be
upgraded to at least important - or even better, fix them.


SELinux support
===============

Etch will not ship with SELinux turned on by default.  However, Etch 
shall ship with all the SELinux components required for users to run an
SELinux enabled machine, including an optimized version of the reference
policy.  The policy would be tested to work for a bare-bones "standard"
installation (that is, a base system with all packages with priority
standard or higher) and with some popular packages like apache, bind,
postfix and sendmail (in other words, most common server packages).
 
At this point, some non-SELinux packages (coreutils, pam, sysvinit) are
running with older SELinux patches. These need to be updated, and 
respective bugs will be filed in the next few days.


Timeline
========

Reviewing our old schedule:

| N-118  = Sat 30 Jul 06:
|
|     freeze essential toolchain, kernels
|     [ kernel freeze is probably a bit delayed ]

The kernel hasn't been frozen yet, but the toolchain is now largely in
the shape we want to see for etch.

|     RC bug count less than 200

We have failed to reach this target. Because of this, more aggressive
removal hints are being used to get release critical buggy packages out
of testing. If you want to see your (or someone else's) package in etch,
help getting it fixed.

| N-110  = Mon  7 Aug 06:
|
|    freeze base, non-essential toolchain (including e.g. cdbs)

This hasn't happened yet, but is being prepared and will be in place by
the next few days.

|   review architectures one more time (only remove broken archs)

No architectures have been removed from the release set. It looks like
etch will be released with 11 architectures - the same as sarge, but
without m68k and with amd64.  The final status of sparc and hppa has
to be checked.


And now to our next steps:

    Fri 11 - Sun 13 Aug 06:

    real-life BSP in G�tersloh, Germany for the debian-installer
    real-life BSP from DebianM�xiko, Mexico City, Mexico
    and online BSP world-wide
    
    
N-105  = Mon 14 Aug 06:

    d-i RC [directly after base freeze]

    RC bug count less than 170

    
    Fri 8 - Sun 10 Sep 06:

    real-life BSP in Wien (Vienna), Austria
    real-life BSP in Z�rich, Switzerland
    and online BSP world-wide
    
    
    Fri 6 - Sun 8 Oct 06:

    real-life BSP in Espace Autog�r� des Tanneries, Dijon, France,
    and online BSP world-wide
    
    
N-45   = Wed 18 Oct 06:

    general freeze [about 2 months after base freeze, d-i RC]
    review architectures last time (only remove broken archs)
    final d-i build; chance to change the kernel version

    RC bug count less than 80

    
N      = Mon  4 Dec 06:
    release [1.5 months for the general freeze]

    no RC bugs left!


Release blockers
================

Still open from our release blockers list:

 * amd64 as an official arch (and the mirror split as a pre-condition
   for that)

This is almost done. We are still waiting for the transition of the new
neon version to testing (it's still being blocked by perl's FTBFS), which
should then bring down the uninstallability count on amd64 to 0.


 * sorting out docs-in-main vs. the DFSG

We have seen some promising development here, and only a few packages need
to be updated for this.  Sadly, the remaining packages include glibc,
automake and emacs21.  Please try to help out for those.


 * sorting out non-free firmware

There is at least one issue, qlogic FC host firmwares, which are needed
to initialise the devices. One cannot access the attached storage without it.
Unfortunately, we still need some more development time here. Although this
doesn't sound like a large issue, it has the potential to delay the release.


 * secure apt

The needed functionality is already tested and included in etch's apt
version.  We still need to work out how to do key management, but there
has been some progress lately.


So much for now.  Thanks for your support.


Cheers,
-- 
Andi
Debian Release Team

[1] http://bjorn.haxx.se/debian/testing.pl?package=$PACKAGE

[2] http://www.us.debian.org/doc/developers-reference/ch-pkgs.en.html#s-nmu

[3] http://ftp-master.debian.org/~he/base-freeze-packages

[4] http://lists.debian.org/debian-devel-announce/2006/08/msg00004.html
