To: debian-devel-announce@lists.debian.org
Subject: Bits from the release team: Release schedule; the RT needs YOU

Heya,

As you may have noticed, updates from the release team have been scarce
in the past few months. We are trying to perform better in the future,
but would be glad to get help.

Release schedule
================
We hoped to freeze in March, but the current number of RC bugs makes
this highly unlikely. From our experiences with previous release cycles,
we wish to freeze only after the number of these bugs has dropped below
the mark of 300. As you can see on the usual overview pages [RC-Bugs],
we are currently far away from this goal.

Work towards fixing these bugs is greatly appreciated. We will use our
release superpowers to aggressively remove leaf packages from testing
(in fact, another round of removals happened on the weekend). Please
check if packages you maintain or use are removal candidates, for
example by running ``rc-alert'' (from the devscripts package).

There are still some ongoing transitions, but we are confident of finishing
them as part of our usual day-to-day business. Some smaller transitions
are still in the queue, but should be finished fast as soon as they get
the green light.

Request for help
================
As you may have noticed, the release team has been notably less active in
the last six months. If you want to invest more time into making Squeeze
the best Debian release ever, feel free to send a mail or contact us in
IRC. We need more manpower to coordinate transitions and bug fixes, so
please consider lending a helping hand.

Cheers,
NN

Footnotes:
 [RC-Bugs] http://bugs.debian.org/release-critical/
           http://bts.turmzimmer.net/
-- 
http://release.debian.org
Debian Release Team
