To: debian-devel-announce@lists.debian.org
Subject: Release update: debian-installer, upload targets, kernels, infrastructure

Hello, world,



Kernel updates
--------------

A kernel/d-i team meeting was recently held to review the status of
2.6.8.  The team leads involved eventually decided to stay with kernel
2.6.8 and 2.4.27, rather than bumping the 2.6 kernel to 2.6.10.  This
decision was made upon review of the known bugs in each of the 2.6
kernel versions; despite some significant bugs in the Debian 2.6.8
kernel tree, these bugs were weighed against the additional delays that
a kernel version bump would introduce in the schedule for
debian-installer RC3.

As it happens, preparing 2.4 and 2.6 kernels with the security fixes for
all architectures took roughly two months from start to finish, during
which time preparation of the next debian-installer release candidate
has been entirely stalled.  A two-month turnaround for critical security
fixes really isn't acceptable, and makes it difficult to progress
towards a release.  It's for this reason that all architectures are
required to be synced to the same kernel version for sarge, but even so,
more per-architecture kernel help is needed, particularly for the sparc
and the arm port.


Debian Installer RC3
--------------------

The current plan of the Installer team for the RC3 includes these dates:

28 Feb	all udeb changes should be complete
        all updated kernels must be in testing
1 Mar	arrive at final go/no-go list for which udebs will be updated in rc3
2 Mar	rc3 udebs synced to testing (some rc2 images may break)
2 Mar	debs that were waiting on rc3 should reach testing now
        (parted, debootstrap)
        finish all needed changes to debian-installer package
        (manual, build system)
        begin debian-installer package test build
3 Mar	merge updates to sarge branch of svn repository, where necessary
7 Mar	debian-installer test build finishes (approximate)
        test period
19 Mar	final netinst and businesscard CD builds
        begin full CD builds
20 Mar	final testing
22 Mar	web site updates
23 Mar	rc3 release

Please expect that some days off is always possible.


RC bug count, BSP
-----------------

The RC bug count keeps falling, partly as a result of last weekend's
bug-squashing party.  Despite the arrival of a lot of new bugs (missing
copyright for GFDLed man pages), the bug-squashers managed to get the RC
bug count down from 120 to less than 100.  That's another all-time low since
we started to count RC bugs.

Of course, this is not enough.  We need to get the number of RC bugs
significantly lower for release, so please continue to help with fixing
the packages you care about.  If you are interested in helping, but
don't have experience with bug-squashing, there is a new primer
available at [1] that you may find helpful.


Status of security bugs in testing
----------------------------------

Outside of the numerous kernel rebuilds required, sarge seems to be in
good shape security wise: Joey Hess has been tracking release-critical
security issues for testing, with assistance from both the Security Team
and the new Debian testing security team, and a running account of known
security vulnerabilities in testing can now be found at [2].  The count
naturally varies from day to day, but seems to have been holding between
10 and 30 now.

In the past, some of these security bugs were fixed in unstable and
merely waiting to propagate to testing, largely blocked by missing
builds for the mipsel architecture.  For a while last month, packages
out-of-date on that architecture were ignored during the testing
promotion process to speed this up.  Since then, another buildd has been
added, so mipsel is back to its former status in testing and is no
longer holding up bug fixes.


testing-proposed-updates, testing-security
------------------------------------------

Currently, some further tweaks are being made to optimize the build
daemons' interface to wanna-build, after which there should only be the
routine maintenance matter of setting up the wanna-build databases for
these queues and configuring/updating the testing chroots on the buildds.

In short, this means that the number one blocker on the release is close
to resolution, and the testing-security and testing-proposed-updates
queues will both be fully operational in the foreseeable future.  We
should now be focusing as much as possible on stabilizing the existing
packages in the archive, to cut down on the number of RC bugs we'll have
to find and fix after the freeze starts.


Other issues
------------

In an effort to reduce the number of library packages, db4.1 and
vacation are no longer part of base.  Also, some outdated libraries like
gnutls7, gnutls10 and libgcrypt are pending removal from sarge, although
some of them are waiting for the next debian-installer release candidate
first.

If you encounter anything else requiring attention by the release team,
please don't hesitate to bring it up.  As the release approaches, we
have a chance to pay attention to smaller issues.

A rather nasty problem is that the last gtk+2.0 upload was too large for
some of our buildds. However, gtk+2.0 sits on one end of deep dependency
chain, so more than one package is blocked by this.  Currently, the
buildd maintainers are working on a permanent fix, and the gtk+2.0
maintainers consider a temporary solution in reducing the build size.


NMU policy
----------

As we're getting closer to release, we want to rehash our NMU policy.
We are in the bug hunting season, help with resolving release critical
or important bugs is appreciated.  You should endeavor to reach the current
maintainer of the package first; they might be just about to upload a fix
for the problem, or have a better solution present.

NMUs should be made to assist a package's maintainer in resolving bugs.
Maintainers should be thankful for that help, and NMUers should respect the
decisions of maintainers, and try to help the maintainer by their work. 

 * Make sure that the package's bugs that the NMU is meant to address
   are all filed in the Debian Bug Tracking System (BTS).  If they are
   not, submit them immediately.  If you have a patch, submit the patch.
   Mark any bug with submitted patch by the patch-tag.

 * Wait a few days for the response from the maintainer.

 * If there is no answer from the maintainer, send them a mail
   announcing your intent to NMU the package.  Prepare an NMU as
   described in the developers reference [3], and test it carefully on
   your machine.  Double check that your patch doesn't have any
   unexpected side effects.  Make sure your patch is as small and as
   non-disruptive as it can be.

 * We are in the bug hunting season till release of sarge, so you may
   upload the fixed package directly.  Send the final patch to the
   maintainer via the BTS.

 * Follow what happens, you're responsible for any bug that you
   introduced with your NMU.  You should probably use The Package
   Tracking System to stay informed of the state of the package after
   your NMU.

Please remember: Though we are in the bug hunting season, you need to
contact the maintainer first.  And, please remember to be polite.  We
are all volunteers and cannot dedicate all of our time to Debian.



Upload targets
--------------

Those changes that are still needed for sarge should continue to be
uploaded according to the following guidelines:

  - If your package is frozen, but the version in unstable contains no
    changes unsuitable for testing, upload to unstable only and contact
    debian-release@lists.debian.org if changes need to be pushed through
    to testing.

  - If your package is frozen and the version in unstable includes
    changes that should NOT be part of sarge, contact
    debian-release@lists.debian.org with details about the changes you
    plan to upload (diff -u preferred) and, with approval, upload to
    testing-proposed-updates.  Changes should be limited to translation
    updates and fixes for important or RC bugs.

  - If you need to do a bug fix directly in sarge, you will need to
    upload to testing-proposed-updates as well, with the same
    requirements as for frozen packages.

  - All other package updates should be uploaded to unstable only.
    Please use urgency=high for uploads that fix RC bugs for sarge
    unless you believe there is a significant chance of breakage in the
    new package.

If you want to increase the urgency of an already uploaded package,
please speak with the release team; this can be sorted out without the
need for another upload.



Cheers,
-- 
Andi Barth
Debian Release Team

[1] http://people.debian.org/~vorlon/rc-bugsquashing.html
[2] http://merkel.debian.org/~joeyh/testing-security.html
[3] http://www.debian.org/doc/developers-reference/ch-pkgs.en.html#s-nmu
