THIS IS A DRAFT
THIS IS A DRAFT
THIS IS A DRAFT

From: Steve Langasek <vorlon@debian.org>
To: debian-devel-announce@lists.debian.org
Subject: Release update

Hello world.

One GR, some flame wars, and a debian-installer test candidate later,
it's time for another update on the status of the sarge release.

First, some bad news.  It appears to fall to me to inform you all that
Anthony Towns has decided to step down as release manager for Debian.
Those of you who have followed mailing list discussions over the past
months should not find it difficult to imagine why; the recrimination
and hostility towards some of our most dedicated developers has been
appalling.  Debian is perhaps the most successful community-driven
distribution of all time, but if we're going to be able to release sarge
we need to start acting like a community again and show some respect
for each other.

And thank you, AJ, for hanging in as long as you did.

Colin Watson and I are trying our best to fill the post that AJ has left
behind.  It has not been a smooth transition.  I want to apologize to
everyone for the suddenness of the base freeze announcement; faced with
a difficult choice between freezing a not-quite-ready base and sending
the debian-installer team through another intensive release cycle, we
made the choice that seemed correct at the time.  We hope you will bear
with us as we take our first steps on this tightrope without a net, and
we hope you'll let us know when we're faltering.  Just as
ajt-woody-sucks@debian.org was available for feedback about problems
with the woody release, please use debian-release@lists.debian.org for
reporting any problems we need to know about with the sarge release --
including RC bugs that are fixed in unstable but not in testing.


Now, on to the promised timeline.  We have several major bugs in base
and in the toolchain that are still being worked out; this makes
predicting a timeline for the freeze a little more difficult than we'd
like it to be, but here we go anyway.


  31 July 2004
  ~215 RC bugs
  Last uploads of base packages to unstable; end of the "normal"
  development cycle

Because of problems in a few key base packages that could not be fixed
by the original deadline, the freeze has been pushed back two more days
to lighten the load on testing-proposed-updates.  Of course, there are
other packages after those whose bugs it would also be easier to fix
through unstable, but two days is as much slack as the debian-installer
schedule <http://lists.debian.org/debian-boot/2004/07/msg01541.html>
appears to allow.

Base and standard libraries in unstable are still frozen with regards to
their shlibs, so that lower-priority packages built against them can
still propagate to testing.


  2 August 2004
  ~215 RC bugs
  Hard freeze of base+standard

At this point, all the changes to base and standard packages that can be
allowed in from testing have been allowed in.  Fixes for RC and
important bugs, as well as updated package translations, are allowed
through uploads to testing-proposed-updates with hand-approval by the
release team.

Among the bugs remaining to be fixed in base+standard post-freeze are a
pair of RC bugs in binutils affecting hppa and mipsel.  These bugs will
need to be resolved in short order for the freeze to proceed, since not
having a releasable toolchain in testing will hold up
testing-proposed-updates and security support for sarge.  This problem
is being worked on, and if it's not fixed by then, we should at least
know more by the middle of this week.

If anything, the RC bug count will be going up at this time due to the
libtiff4 transition that's in progress.  With the help of prompt
maintainer responses and NMUs, the count will hopefully come right back
down again.

As has been noted repeatedly, we want to freeze for as short a time as
possible.  This means that, with the freeze started, we need to be
fixing RC bugs as quickly as possible to keep moving towards the
release.  With over 200 release-critical bugs left in testing, this is a
tall order, but the on-going BSPs appear to be doing their job, and this
number is already well down from where it was a month ago.

Non-base packages with RC bugs will also be removed aggressively from
sarge after this point.  Please fix your RC bugs promptly; RC bugs
should not be staying open for longer than a week.  If you are having
trouble fixing an RC bug, please contact debian-release, or ask for help
in #debian-bugs on irc.freenode.net.


  3 August 2004
  ~300 RC bugs
  Debian-installer RC1 released

With the base system frozen, CD images of d-i RC1 will be made available
within a day or two afterward.  Developers, and users with spare
hardware, will begin testing these images for release-worthiness.  The
release schedule allows time for another d-i release in late August to
fix any release-critical problems found.

Around this time, the Debian CD team starts to work out any bugs in the
full-CD sets (like CD #1 containing KDE but not X).  Owing to the amount
of time required for a full CD build, this process may take a while.

Based on feedback from d-i testing, the installation manual is refined
and prepared for release.


  8 August 2004
  ~200 RC bugs
  testing-proposed-updates, testing-security working for all
  architectures
  Official security support for sarge begins

Assuming the toolchain is in order, we can hope to have autobuilders for
testing-proposed-updates and testing-security in working order for all
architectures by this point.  The testing-proposed-updates queue will
already be in use for uploads of RC bugfixes, but up to now
testing-security is not in use.  Now that it's ready, the security team
can begin providing security support for sarge.  The sooner this can
actually happen, the better.

With security support in place, adventurous users can begin testing the
upgrade path from woody to sarge.  Their feedback will be used for
further bugfixing of packages, and to start preparing release notes.


  10 August 2004
  185 RC bugs
  Last call for low-urgency uploads

This is the last day for uploads of low-priority changes to get into
sarge.  Uploads after this date will miss the cutoff for the sarge
freeze.

This allows only 2 days after the start of upgrade testing for low
priority fixes to get uploaded if they're targetted for sarge.  Of
course, bugs that warrant uploads at a higher urgency can be fixed after
this date; including t-p-u uploads for RC bugs.


  21 August
  100 RC bugs
  d-i TC2 if needed
  Freeze time!

We need to get most of our RC bugs fixed in the first three weeks of
August. The ones that are left after this are going to be the *hard*
ones, so if we don't have the bug count down to expected levels by
mid-August, we'll have to re-evaluate the freeze timeline.

But if all goes well, we'll be ready to freeze the rest of the archive
at this point.  Changes to base+standard are limited to RC fixes only;
fixes for RC bugs and translation updates are allowed for the rest of
the archive via testing-proposed-updates.

Once the archive is frozen, this will also be the time for a d-i final
if necessary -- bug-fixes-only, based on feedback from Release Candidate 1.


  12 September
  0 RC bugs

Any remaining release-critical bugs will be fixed through uploads to
testing-proposed-updates or by removals from sarge.

With a final cut of the installer in the bag, the beginning of September
will also see fixes to any remaining CD generation problems, as well as
final tweaks to the installation manual and release notes.

On the 12th, we will re-evaluate where things are, and pick a date for
the full release.


  15 September
  Release

And as far as the release itself goes, this looks like a good target to
shoot for.


For those who want a clearer idea of whether we're meeting these goals,
in addition to d-i status and the RC bug count at
http://bugs.debian.org/release-critical, information about major
outstanding issues will be posted at
<http://release.debian.org/sarge.html>.  If you know of other blockers
that should be listed here, or if you have RC bugs that you're having a
hard time fixing, please contact debian-release@lists.debian.org.

And off we go.  To infinity^Wsarge and beyond!

Cheers,
-- 
Steve Langasek                                     [vorlon@debian.org]
Debian Release Assistant, on behalf of the release team
