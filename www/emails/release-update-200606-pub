From: Marc Brockschmidt <he@debian.org>
Subject: New Release Update
To: debian-news@lists.debian.org

------------------------------------------------------------------------
The Debian Project                                http://www.debian.org/
New Release Update published             debian-release@lists.debian.org
July 17th, 2006
------------------------------------------------------------------------

Today, an updated plan for the release of the next stable version of Debian,
codenamed "Etch", has been published. Etch has been assigned 4.0 as version
number. 
The planned release date of December 2006 still looks realistic.
Two architectures (s390, sparc) were added back to the release architectures
for etch, which now contains 11 architectures (alpha, amd64, arm, hppa,
i386, ia64, mips, mipsel, powerpc, s390, sparc). Etch will be shipped
without 2.4 kernels and support for 2.4 has been deprecated.

Minor items in the release update include the transition to the current
python version 2.4, updates about the kernel selection (likely 2.6.17 for now,
there is the possibility to update the kernel in October to a newer version, if
appropriate), and the completed update to gcc 4.1 as default compiler.
The release update also clarified the distinction between Release Blockers
and Release Goals, and lists all of them. The full release update is available
online at http://release.debian.org/20060717

The current stable version of Debian (codename Sarge, Version 3.1) was
published in June 2005, and has been updated since then by point releases.

About Debian
------------

Debian GNU/Linux is a free operating system, developed by more than
thousand volunteers from all over the world who collaborate via the
Internet.  Debian's dedication to Free Software, its non-profit nature,
and its open development model make it unique among GNU/Linux
distributions.

The Debian project's key strengths are its volunteer base, its dedication
to the Debian Social Contract, and its commitment to provide the best
operating system possible.


Contact Information
-------------------

For further information, please visit the Debian web pages at
<http://www.debian.org/> or send mail to <debian-release@lists.debian.org>.
