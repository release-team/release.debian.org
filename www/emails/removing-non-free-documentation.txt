Hi.

Since one of our release goals for etch is to remove any documentation
from main that doesn't follow the DFSG (for brevity called "non-free
documentation" below), here some comments from the release team on the
issue.

Contents:

1. Things to do
2. Dealing with non-free documentation in one of your packages
3. Filing bugs about non-free documentation

---------------------------------------------------------------------

1. Things to do:

 - Identifying non-free documentation in main and making a mass-bug filing.
   This should happen ASAP.
   
   My current plan is to do 1) a grep run over all debian/copyright files
   and search for common non-free licenses and then 2) do the same with
   files whose license is often forgotten to be mentioned in
   debian/copyright, e.g. man pages and info files. If you know of a
   package with non-free documentation that will surely not be found
   by one of these steps (either because the license is a non-standard
   one or because it isn't mentioned in debian/copyright and only mentioned
   in files one isn't likely to check) please feel free to file a bug about
   that right now. Please read the section "filing bugs about non-free
   documentation" of this mail though.
   
   Known non-free documentation licenses are:
    - GFDL (at least up the current version 1.2)
    - CC licenses (at least up to the current version 2.5)
    - OPL and OpenContent License
    - the current license for RFCs (see also #199810)
   There is also some documentation under non-free software licenses
   and under non-free home-made licenses.

 - Implementing support for the BTS version tracking in britney (i.e. the
   script that is responsible for the testing migration). Since bugs
   about non-free documentation are likely to affect testing and unstable
   versions of a package they should not hinder testing propagation of
   the packages in most cases. Until britney supports version tracking
   there is really no good way to achieve this. The code for that is
   almost ready though and just needs some adjustments on the BTS side to
   go live.

 - Setting up an overview page for the release team (and others) to
   track the status of the filed bugs. We plan to do that with the
   new usertags feature directly in the BTS but please refer to
   http://release.debian.org/removing-non-free-documentation for now
   since there is no good way yet to give you a nice URL to the page
   directly. We will post any useful URLs there.

---------------------------------------------------------------------

2. Dealing with non-free documentation in one of your packages

So what should you do if you find non-free documentation in one of
your packages?

 1) Don't panic!
  
 Take your time to deal with the issue. Now you still have it!
 But you will have to start _now_. The fact that we haven't
 yet removed packages from testing for RC bugs about non-free
 documentation doesn't mean we won't doing it soon.

 So if you haven't discussed the problem with your upstream yet,
 if you plan to rewrite the documentation and haven't started yet,
 if you want to ask for help with convincing your upstream or with
 rewriting the documentation and haven't done it yet, DO IT NOW!

 2) Identify the amount and sort of affected documentation and
    choose a solution

 If the affected documentation is really small (e.g. a very minimal man
 page) you might consider rewriting it completely. In some cases this
 might actually be faster than to try to contact the original author
 and asking for a relicensing.

 If the affected documentation is closely related to a piece of software
 (and probably packaged with it), e.g. a man page for a executable binary
 or a reference manual for a library, try ask upstream to relicense it
 (or at least dual-license it) under the same terms as the software
 itself. This is probably a good idea in any case because it allows
 easier copying of material back-and-forth between the source code
 and documentation

 In any other case you might ask the author for a relicensing (or
 at least dual-licensing). There is no known license out there
 though that was specifically designed for documentation and
 follows the DFSG. Something like a BSD-style license should be
 fine but the lack of "copyleft" might be disliked by the author.

 We hope that we will get some more free documentation licenses in
 time for etch (both on the side of the GFDL and the CC licenses)
 but we can't depend on that.

 If the author objects to relicensing the documentation you have essentially
 three choices: 1) removing the documentation completely,
 2) moving the documentation to non-free, 3) rewriting it.

 In any case DOCUMENT YOUR CHOSEN SOLUTION IN THE BUG REPORT. If there
 doesn't exist a bug report about your case consider filing it.

---------------------------------------------------------------------

3. Filing bugs about non-free documentation

 I began to retitle bugs about non-free documentation to use a common
 meta-tag in the subject. After the introduction of the usertags I
 was urged to look into that as an alternative way of doing it and
 it proved to be very useful.

 So here the new way: After filing a bug about non-free documentation
 please add an usertag "nonfree-doc" and one usertag that describes the
 license, like "gfdl", "cc", "opl" for the common licenses or something more
 descriptive like "non-commercial" or "unmodifiable" for custom licenses.

 Please add these usertags under the user debian-release@lists.debian.org
 
 You can do this with a mail to request@bugs.debian.org (or
 control@bugs.debian.org) like this:

<---------------------------------------------->
To: request@bugs.debian.org

user debian-release@lists.debian.org
usertag 123456 + nonfree-doc gfdl
<---------------------------------------------->

 If you want to add something to your bug's subject like [NONFREE-DOC]
 to make it easier recognisable, just do it, it isn't mandatory though.
