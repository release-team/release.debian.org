# vim:set et sw=4:

amd64:
    available: yes
    portbox: barriere
    users: 23000+
    installer: d-i
    archive-coverage: 99
    archive-uptodate: 99
    upstream-support: yes
    buildds:
        - x86-conova-01
        - x86-conova-02
        - x86-csail-01
        - x86-grnet-01
        - x86-ubc-01
        - x86-ubc-02
    buildd-dsa: yes
    autopkgtest: yes
    concerns-rm: no
    concerns-srm:
    concerns-dsa: no
    concerns-sec: no
    candidate: yes

arm64:
    available: yes
    portbox: amdahl
    porters:
        - Vagrant Cascadian
        - Wookey
    users: "?"
    installer: d-i
    archive-coverage: 99
    archive-uptodate: 99
    upstream-support: yes
    buildds:
        - arm-arm-01
        - arm-arm-03
        - arm-arm-04
        - arm-conova-01
        - arm-conova-02
        - arm-conova-04
        - arm-ubc-01
        - arm-ubc-02
        - arm-ubc-03
    buildd-dsa: yes
    autopkgtest: yes
    concerns-rm: no
    concerns-srm:
    concerns-dsa: "yes: unstable and ageing hardware"
    concerns-sec: no
    candidate: "?"

armel:
    available: yes
    portbox: abel
    porters:
        - Adrian Bunk
        - Wookey
    users: "?"
    installer: d-i
    archive-coverage: 97
    archive-uptodate: 99
    upstream-support: "glibc has trouble finding qualified persons to implement security fixes,<br>
        might be special because the use of the libatomics library is mandatory"
    buildds:
        - arm-arm-01
        - arm-arm-03
        - arm-arm-04
        - arm-conova-01
        - arm-conova-02
        - arm-conova-04
        - arm-ubc-01
        - arm-ubc-02
        - arm-ubc-03
        - arm-ubc-04
        - arm-ubc-05
        - arm-ubc-06
        - hartmann
    buildd-dsa: yes
    autopkgtest: yes
    concerns-rm: "some builds are hitting the address space limit"
    concerns-srm:
    concerns-dsa: "yes: unstable and ageing hardware"
    concerns-sec: no
    candidate: "?"

armhf:
    available: yes
    portbox: harris
    porters:
        - Vagrant Cascadian
        - Wookey
    users: "?"
    installer: d-i
    archive-coverage: 98
    archive-uptodate: 99
    upstream-support: "glibc has trouble finding qualified persons to implement security fixes"
    buildds:
        - arm-arm-01
        - arm-arm-03
        - arm-arm-04
        - arm-conova-01
        - arm-conova-02
        - arm-conova-04
        - arm-ubc-01
        - arm-ubc-02
        - arm-ubc-03
        - arm-ubc-04
        - arm-ubc-05
        - arm-ubc-06
        - hartmann
    buildd-dsa: yes
    autopkgtest: yes
    concerns-rm: "some builds are hitting the address space limit"
    concerns-srm:
    concerns-dsa: "yes: unstable and ageing hardware"
    concerns-sec: no
    candidate: "?"

i386:
    available: yes
    portbox: barriere
    porters:
        - Adrian Bunk
    users: 92000+
    installer: d-i
    archive-coverage: 98
    archive-uptodate: 99
    upstream-support: "no: missing CPU security mitigations in Linux kernel"
    buildds:
        - x86-conova-01
        - x86-conova-02
        - x86-csail-01
        - x86-grnet-01
        - x86-ubc-01
        - x86-ubc-02
    buildd-dsa: yes
    autopkgtest: yes
    concerns-rm: "only one porter has volunteered,<br>
        some builds are hitting the address space limit"
    concerns-srm:
    concerns-dsa: no
    concerns-sec: no
    candidate: "?"

loong64:
    available: yes
    portbox: shenzhou
    porters:
        - Dandan Zhang (!DD)
        - John Paul Adrian Glaubitz
    users: thousands of information partners
    installer: d-i
    archive-coverage: 96
    archive-uptodate: 99
    upstream-support: yes
    buildds:
        - loong64-loongson-01
        - loong64-loongson-02
        - loong64-loongson-03
        - loong64-loongson-04
        - loong64-loongson-05
        - loong64-loongson-06
    buildd-dsa: no
    autopkgtest: yes
    concerns-rm: "?"
    concerns-srm: "?"
    concerns-dsa: "?"
    concerns-sec: "?"

mips64el:
    available: yes
    portbox: eller
    porters:
        - YunQiang Su
    users: "?"
    installer: d-i
    archive-coverage: 97
    archive-uptodate: 99
    upstream-support: "no: no upstream support in GCC,<br>
        unaddressed test failures in binutils"
    buildds:
        - mipsel-manda-04
        - mipsel-manda-05
        - mipsel-osuosl-01
        - mipsel-osuosl-02
        - mipsel-osuosl-03
        - mipsel-osuosl-04
        - mipsel-osuosl-05
        - mipsel-sil-01
    buildd-dsa: yes
    autopkgtest: no
    concerns-rm: "yes: upgrading buildds is impossible due to kernel bugs<br>
        kernel bugs are hitting some of the buildds making them unusable<br>
        builders are extremely slow (also per kernel team),<br>
        only one porter has volunteered,<br>
        lack of autopkgtest infrastructure,<br>
        future availability of hardware"
    concerns-srm:
    concerns-dsa: "yes: non-server-grade hardware<br>
        time sink for DSA and hosters"
    concerns-sec: no
    candidate: "?"

ppc64el:
    available: yes
    portbox: plummer
    porters:
        - Frédéric Bonnard
    users: "?"
    installer: d-i
    archive-coverage: 98
    archive-uptodate: 99
    upstream-support: yes
    buildds:
        - ppc64el-osuosl-01
        - ppc64el-osuosl-02
    buildd-dsa: yes
    autopkgtest: yes
    concerns-rm: "only one porter has volunteered"
    concerns-srm:
    concerns-dsa: "hosting is not great, effectively not redundant.<br>
        Unclear what our options are regarding purchasing hardware."
    concerns-sec: no
    candidate: "?"

riscv64:
    available: yes
    portbox: debian-riscv64-porterbox-01
    porters:
        - Aurelien Jarno
        - Manuel A. Fernandez Montecelo
        - Adrian Bunk
        - Adam Borowski
    users: "?"
    installer: d-i
    archive-coverage: 96
    archive-uptodate: 99
    upstream-support: yes
    buildds:
        - rv-manda-01
        - rv-manda-02
        - rv-manda-03
        - rv-manda-04
        - rv-osuosl-01
        - rv-osuosl-02
        - rv-osuosl-03
        - rv-osuosl-04
        - rv-osuosl-05
    buildd-dsa: yes
    autopkgtest: yes
    concerns-rm: no
    concerns-srm:
    concerns-dsa: "?"
    concerns-sec: "?"
    candidate: "?"

s390x:
    available: yes
    portbox: zelenka
    porters:
        - Dipak Zope (!DD)
        - Rajendra Kharat (!DD)
    users: "?"
    installer: d-i
    archive-coverage: 97
    archive-uptodate: 99
    upstream-support: yes
    buildds:
        - zandonai
        - zani
    buildd-dsa: yes
    autopkgtest: yes
    concerns-rm: no
    concerns-srm:
    concerns-dsa: "rely on sponsors for hardware (mild concern)"
    concerns-sec: no
    candidate: "?"
